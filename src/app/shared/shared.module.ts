import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatInputModule} from '@angular/material/input';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import { ProductFormDialogComponent } from '../add-product/product-form-dialog/product-form-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  declarations: [
    ProductFormDialogComponent,
  ],
  imports: [
    CommonModule,
    MatSidenavModule,
    MatInputModule,
    MatSlideToggleModule,
    MatCardModule,
    MatSelectModule,
    MatIconModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
    ReactiveFormsModule,
  ],
  exports: [
    MatSidenavModule,
    MatInputModule,
    MatSlideToggleModule,
    MatCardModule,
    MatSelectModule,
    MatIconModule,
    FormsModule,
    MatButtonModule,
    ProductFormDialogComponent,
    MatDialogModule,
    ReactiveFormsModule,
  ],
  entryComponents: [
    ProductFormDialogComponent
  ]
})
export class SharedModule { }
