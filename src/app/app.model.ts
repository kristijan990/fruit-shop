export class Product {
    constructor(
        public image ?: string,
        public name ?: string,
        public price ?: number,
        public quantity ?: number,
        public sold ?: number,
        public growth ?: number,
        public purchasedQuantity ?: number,
    ) {}
}