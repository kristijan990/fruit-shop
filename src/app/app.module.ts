import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ShopModule } from './shop/shop.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AddProductModule } from './add-product/add-product.module';


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ShopModule,
    AddProductModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
