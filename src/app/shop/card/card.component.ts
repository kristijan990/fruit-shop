import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from '../../app.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() product : Product;
  @Input() cardDesign: string;
  @Input() fontSize: number;

  @Output() productList = new EventEmitter<Product>();

  quantity: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  decreaseQuantity() {
    this.quantity -- ;
  }

  increaseQuantity() {
    this.quantity ++ ;
  }

  buy() {
    this.product.purchasedQuantity = this.product.purchasedQuantity ? this.product.purchasedQuantity : 0;
    this.product.purchasedQuantity += this.quantity;
    this.productList.emit(this.product);
    this.product.sold = this.product.sold + this.quantity;
    this.quantity = 0;
  }
  
}
