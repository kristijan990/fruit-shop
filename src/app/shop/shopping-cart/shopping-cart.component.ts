import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Product } from '../../app.model';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  @Input() shoppingCart : Product[];

  purchasedItems: Product[] = [];

  constructor(
    private cdref : ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewChecked() {
    this.purchasedItems = this.shoppingCart.filter(item => item.purchasedQuantity);
    console.log(this.purchasedItems)
    this.cdref.detectChanges();
  }

  calculateTotal() {
    let total: number = 0;

    this.purchasedItems.forEach(element => {
      total += element.price * element.purchasedQuantity;
    })

    return total;
  }

  removeItem(i: number) {
    this.purchasedItems[i].quantity = this.purchasedItems[i].purchasedQuantity + this.purchasedItems[i].quantity;
    this.purchasedItems[i].purchasedQuantity = 0;
  }

}
