import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ProductFormDialogComponent } from 'src/app/add-product/product-form-dialog/product-form-dialog.component';
import { Product } from 'src/app/app.model';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {

  productList: Product[] = [];
  cardDesign: string;
  fontSize: number = 14;

  dialogRef: any;
  templateProduct: Product = new Product();

  constructor(
    public dialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.productList = [
      {
        "image" : "https://i.ibb.co/vzyY6h1/1.jpg",
        "name"  : "Purpe Cabagges",
        "price" : 35,
        "quantity" : 20,
        "sold" : 14,
        "growth" : 0
      },
      {
        "image" : "https://i.ibb.co/hsNPYYz/2.jpg",
        "name"  : "Oranges",
        "price" : 27,
        "quantity" : 10,
        "sold" : 10,
        "growth" : 1
      },
      {
        "image" : "https://i.ibb.co/sC1V3qC/3.jpg",
        "name"  : "Broccoli",
        "price" : 55,
        "quantity" : 12,
        "sold" : 8,
        "growth" : 0
      },
      {
        "image" : "https://i.ibb.co/G0FmxPB/4.jpg",
        "name"  : "Red Peppers",
        "price" : 30,
        "quantity" : 15,
        "sold" : 5,
        "growth" : 0
      },
      {
        "image" : "https://i.ibb.co/mStQqdX/5.jpg",
        "name"  : "Potatoes",
        "price" : 20,
        "quantity" : 30,
        "sold" : 30,
        "growth" : 0
      },
      {
        "image" : "https://i.ibb.co/f4VSv1j/6.png",
        "name"  : "Bananas",
        "price" : 40,
        "quantity" : 35,
        "sold" : 32,
        "growth" : 1
      },
      {
        "image" : "https://i.ibb.co/sqC5G9q/7.png",
        "name"  : "Apples",
        "price" : 45,
        "quantity" : 25,
        "sold" : 17,
        "growth" : 1
      }
    ];

    this.templateProduct.name = 'Papaya';
    this.templateProduct.image = 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fclipartspub.com%2Fimages%2Fpapaya-clipart-fruit-6.png&f=1&nofb=1';
    this.templateProduct.price = 50;
    this.templateProduct.quantity = 15;
    this.templateProduct.sold = 3;
    this.templateProduct.growth = 1;
  }

  updateList(event: any) {
    let product_index = this.productList.findIndex(item => item.name == event.name);

    this.productList[product_index] = event;
  }

  addProductDialog() {
    this.dialogRef = this.dialog.open(ProductFormDialogComponent, {
      width: '700px',
      data: {
        template: this.templateProduct,
      }
    });

    this.dialogRef.afterClosed().subscribe(
      (result: FormGroup) => {
        if (result) {
          let newProduct: Product = new Product();
          newProduct.name = result.value.name;
          newProduct.image = result.value.image;
          newProduct.price = result.value.price;
          newProduct.quantity = result.value.quantity;
          newProduct.sold = result.value.sold;
          newProduct.growth = JSON.parse(result.value.growth);
  
          this.productList.push(newProduct);
        }
      }
    )
  }

}
