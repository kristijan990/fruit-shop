import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardDesignDirective } from '../card-design.directive';
import { CardComponent } from './card/card.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { SharedModule } from '../shared/shared.module';
import { ShopComponent } from './shop/shop.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    CardComponent,
    CardDesignDirective,
    ShoppingCartComponent,
    ShopComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
  ]
})
export class ShopModule { }
