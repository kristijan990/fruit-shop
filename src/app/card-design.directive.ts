import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appCardDesign]'
})
export class CardDesignDirective {

  @Input('appCardDesign') style : string;

  constructor(private element: ElementRef) { }

  // @HostListener('mouseenter') onMouseEnter() {
  //   this.applyStyle(this.style);
  // }

  // @HostListener('mouseleave') onMouseLeave() {
  //   this.applyStyle(null);
  // }

  ngAfterViewChecked() {
    this.applyStyle(this.style);
  }

  applyStyle(color: string) {
    if (this.style == 'dark')
    {
      this.element.nativeElement.style.backgroundColor = color ? "cadetblue" : 'white';
      this.element.nativeElement.style.color = "white";
      this.element.nativeElement.style.border = "none";
    } else
    if (this.style == "light") {
      this.element.nativeElement.style.backgroundColor = color ? "antiquewhite" : 'whiite';
      this.element.nativeElement.style.color = "black";
      this.element.nativeElement.style.border = "none";
    }
    else {
      this.element.nativeElement.style.backgroundColor = "white";
      this.element.nativeElement.style.color = "black";
      this.element.nativeElement.style.border = "1px solid black"
    }
  }

}
