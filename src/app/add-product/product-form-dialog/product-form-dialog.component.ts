import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from 'src/app/app.model';

@Component({
  selector: 'app-product-form-dialog',
  templateUrl: './product-form-dialog.component.html',
  styleUrls: ['./product-form-dialog.component.css']
})
export class ProductFormDialogComponent implements OnInit {

  productFormGroup: FormGroup;

  // newProduct: Product = new Product();

  constructor(
    public _dialogRef: MatDialogRef<ProductFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any,
    public _formBuilder: FormBuilder,
  ) { 
    this.productFormGroup = this.initProductFormGroup();

    // this.patchFormValues(data.template);
  }

  ngOnInit(): void {
    // this.productFormGroup.disable();
  }

  initProductFormGroup() {
    return this.productFormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      image: ['', Validators.required],
      price: ['', Validators.required],
      quantity: ['', Validators.required],
      sold: ['', Validators.required],
      growth:['', Validators.required],
    })
  }

  saveProduct() {
    // this.newProduct.name = this.productFormGroup.get('name').value;
    // this.newProduct.image = this.productFormGroup.get('image').value;
    // this.newProduct.price = this.productFormGroup.get('price').value;
    // this.newProduct.quantity = this.productFormGroup.get('quantity').value;
    // this.newProduct.sold = this.productFormGroup.get('sold').value;
    // this.newProduct.growth = JSON.parse(this.productFormGroup.get('growth').value);

    this._dialogRef.close(this.productFormGroup);
  }

  patchFormValues(data: Product) {
    this.productFormGroup.patchValue({
      'name': data.name,
      'image': data.image,
      'price': data.price,
      'quantity': data.quantity,
      'sold': data.sold,
      'growth': JSON.stringify(data.growth)
    })
  }

}
