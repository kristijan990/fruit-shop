import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/app.model';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  newProduct: Product = new Product();

  constructor() { }

  ngOnInit(): void {
  }

  saveProduct() {
    console.log('new product', this.newProduct);
  }

}
